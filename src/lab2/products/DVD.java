package lab2.products;

public class DVD extends Product
{
	private String name;
	private String description;
	private int duration;
	
	public DVD(String name, String description, int duration)
	{
		this.name = name;
		this.description = description;
		this.duration = duration;
	}
	

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());		
		result = prime * result + duration;
		return result;
	}


	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DVD other = (DVD) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (duration != other.duration) {
			return false;
		}
		return true;
	}


	public String toString() {
		return "Product [description=" + description + ", name=" + name + "]" + 
	", duration=" + duration ;
	}
}